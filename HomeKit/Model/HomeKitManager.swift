//
//  HomeKitManager.swift
//  HomeKit
//
//  Created by Inaka on 10/28/15.
//  Copyright © 2015 Inaka. All rights reserved.
//

//: This class will have the necessary methods to be used on this application



import HomeKit

public class HomeKitManager: NSObject, HMHomeManagerDelegate {
    
    static let sharedInstance = HomeKitManager()
    
    // MARK: Properties
    
    /// The current 'selected' home.
   public var home: HMHome?
    
    /// The singleton home manager.
    var homeManager = HMHomeManager()
    
    var homes = [HMHome]()
    var rooms = []
    
    var error: NSError?
    
    public var delegate : HMHomeDelegate?
    
    
    
    
    // MARK: Helper Methods
    //TODO:
    
    
    // * metodos que usare para homeKit
    // agregar home
    
    func addHome(name: String) {
        homeManager.addHomeWithName(name) { (home, error) -> Void in
            if ((error) == nil) {
                print("Created Home : \(home!.name)")
                self.home = home
                self.didAddHome(home!)
            } else {
                //TODO manage duplicate in the UI
                print("Error : \(error!.localizedDescription)") //will raise if duplicate
            }
        }
    }
    //** quitar home
    
    func deleteHome(homeObject: HMHome){
        print("home name is \(homeObject.name)")
        homeManager.removeHome(homeObject) { (error ) -> Void in
            if ((error) == nil) {
                print("deleted Home : \(homeObject)")
                self.didRemoveHome(homeObject)
                
            } else {
                
                print("Error : \(error!.localizedDescription)")
            }
        }
    }
    /**
    Adds a new home into the internal homes array and inserts the new
    row into the table view.
    
    - parameter home: The new `HMHome` that's been added.
    */
    func didAddHome(home: HMHome) {
        homes.append(home)
       
        print("Auto Add rooms...")
        //TODO take out of here
        
        /* Now add rooms to this home */
 /*       home.addRoomWithName("Master bedroom",
            completionHandler: {roomAddedToHomeCompletionHandler} )
        
        home.addRoomWithName("Kids' bedroom",
            completionHandler: self.roomAddedToHomeCompletionHandler)
        
        home.addRoomWithName("Gaming room",
            completionHandler: self.roomAddedToHomeCompletionHandler)
   */

    }
    
    func didRemoveHome(home: HMHome) {
        guard let removedHomeIndex = homes.indexOf(home) else { return }
        homes.removeAtIndex(removedHomeIndex)
    }
    //** pick home from list as primary
    private func updatePrimaryHome(newPrimaryHome: HMHome) {
        guard newPrimaryHome != homeManager.primaryHome else { return }
        
        homeManager.updatePrimaryHome(newPrimaryHome) { error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            //TODO some signal to update the View
        }
    }
    
    //*crear rooms
    public  func addRoom(name: String){
        self.home?.addRoomWithName(name, completionHandler: { (thisRoom, error) -> Void in
            if ((error) != nil) {
                print("room added  : \(thisRoom?.name)")
                self.roomAddedToHomeCompletionHandler(thisRoom, error: error)
            } else {
                
                print("Error : \(error!.localizedDescription)")
            }
        })
    }
    
    //Listar rooms
    public    func listRooms()-> [HMRoom]{
        let a = self.home?.rooms
        if (a?.count > 0){
            return a!
        }
        else{
            //it gives warnings if I dont do this. ..>(
            let b = self.home?.roomForEntireHome()
            return [b!]
        }
    }
    public func addRoom2(name: String){
        
    }
    
    //TODO edit this
    func roomAddedToHomeCompletionHandler(room: HMRoom!, error: NSError!)->Void{
        
        if error != nil{
            print("Failed to add room to home. Error = \(error)")
        } else {
        //    if (room.name as NSString).rangeOfString(bedroomKeyword,
             //   options: .CaseInsensitiveSearch).location != NSNotFound{
                    print("A bedroom is added to the home")
                    print("Adding it to the zone...")
                  //  bedroomZone.addRoom(room, completionHandler:
               //         self.roomAddedToZoneCompletionHandler)
           // }
            //else {
                print("The room that is added is not a bedroom")
          //  /}
       }
    }
    //Renonmbrar room
    public func renameRoom(room: HMRoom, withString: String){
        room.updateName(withString) { (error ) -> Void in
            if ((error) != nil) {
                print("room not renamed due  : \(error?.localizedDescription)")
            }
        }
    }
    //Quitar rooms
    public func deleteRoom(which: HMRoom){
        self.home?.removeRoom(which, completionHandler: { (error) -> Void in
            if ((error) != nil) {
                print("room not deleted due  : \(error?.localizedDescription)")
                
            }
        })
    }
    
    //Zones
    
    
    //List accessroises unconfigured
    
    // agregar accesoories
    public func addAccessoory(a: HMAccessory, ToRoom: HMRoom, onHome: HMHome){
        onHome.assignAccessory(a, toRoom: ToRoom) { (error ) -> Void in
            if ((error) != nil){
                print(error?.localizedDescription)
            }
        }
    }
    //** quitar acsriosoe†††††c
    public func removeAccessory(a: HMAccessory){
        home?.removeAccessory(a, completionHandler: { (error) -> Void in
            if ((error) != nil){
                print(error?.localizedDescription)
            }
        })
    }
    //listar accesories
    public func listAccessoriesInRoom(which:HMRoom)->[HMAccessory]{
        return which.accessories
    }
    
    //*** agregar caracteristicas
    //*** quitar caracatirsitcas
    // listar caracateristicas
    public func listServicesOfAccessory(accessory: HMAccessory)->[HMService]{
        print(accessory.services)
        return accessory.services
    }
    //*unblock accessory that was missbehaving
    //TODO poner fecha de bloqueo
    
    //*** agregar service
    //*** quitar service
    //*** agregar triggers
    //*** quitar triggers
    //** crear conjuntos de accessories
    
     func accessoryBrowser(browser: HMAccessoryBrowser,
        didFindNewAccessory accessory: HMAccessory) {
            
            print("Found a new accessory")
            print("Adding it to the home...")
            addAccessory(accessory, completionHandler: {[weak self]
                (error: NSError!) in
                
                let strongSelf = self!
                
                if error != nil{
                    println("Failed to add the accessory to the home")
                    println("Error = \(error)")
                } else {
                    println("Successfully added the accessory to the home")
                    println("Assigning the accessory to the room...")
                    strongSelf.home.assignAccessory(accessory,
                        toRoom: strongSelf.room,
                        completionHandler: {(error: NSError!) in
                            
                            if error != nil{
                                println("Failed to assign the accessory to the room")
                                println("Error = \(error)")
                            } else {
                                println("Successfully assigned the accessory to the room")
                                
                                strongSelf.findServicesForAccessory(accessory)
                                
                            }
                            
                    })
                }
                
                })
            
    }
    
    
    
    
}


// This class will not browse non configures accesories
public class AccessoryBrowserManager: NSObject, HMHomeManagerDelegate, HMAccessoryBrowserDelegate{
    var accessories = [HMAccessory]()
    var home: HMHome!
    var room: HMRoom!
    lazy var accessoryBrowser: HMAccessoryBrowser = {
        let browser = HMAccessoryBrowser()
        browser.delegate = self
        return browser
        }()
    
    var randomHomeName: String = {
        return "Home \(arc4random_uniform(UInt32.max))"
        }()
    
    let roomName = "Bedroom 1"
    
    var homeManager: HMHomeManager!
homemanager.dle
    //
    public func listAcceossiryesUnconfiguredAtHome(_:HMHome){
        
        //        print("This class will not browse uncofigured accessories")
        //        //** buscar accesorios
        let accessoryBrowser = HMAccessoryBrowser()
        accessoryBrowser.delegate = self
        
                startSearchingForNewAccessories()
    }
    
    
    
        //        var listOfAccessories = acce
        //
        //    var displayedAccessories = [HMAccessory]()
        //  //TODO how to add delegate here ???
        //    // accessoryBrowser.delegate = self
        //
        //    public func accessoryBrowser(browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        //        print("did find new accessory")
        //        print("accessories found up to now: \(browser.discoveredAccessories)")
        //        //*** encontrar la lista de las capacidades de accessory
        //    }
        //    public func accessoryBrowser(browser: HMAccessoryBrowser, didRemoveNewAccessory accessory: HMAccessory) {
        //        print("did remove my accessory \(accessory). Perhaps added to home?")
        //    }
        //
        //    func findAvailableAccessories(){
        //     accessoryBrowser.startSearchingForNewAccessories()
        //        //TODO add a timeout
        //        sleep(10)
        //       selfstopSearchingForNewAccessories()
        //    }
        //
        //    func unconfiguredHomeKitAccessoryWithName(name: String) -> HMAccessory? {
        //        for type in accessoryBrowser.discovere
        //            if case let .HomeKit(accessory) = type where accessory.name == name {
        //                return accessory
        //            }
        //        }
        //        return nil
        //    }
}