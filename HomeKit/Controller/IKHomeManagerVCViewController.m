//
//  IKHomeManagerVCViewController.m
//  HomeKit
//
//  Created by Inaka on 10/19/15.
//  Copyright © 2015 Inaka. All rights reserved.
//

#import "IKHomeManagerVCViewController.h"
@import HomeKit;

@interface IKHomeManagerVCViewController () <HMHomeManagerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *homeName;

@property (nonatomic) HMHomeManager *homeManager;
@property (nonatomic, weak) id<HMHomeManagerDelegate> delegate;
@end

@implementation IKHomeManagerVCViewController

#pragma mark LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.homeManager = [[HMHomeManager alloc] init];
    self.homeManager.delegate = self;

    //TODO:  something like
    [self.homeName becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.homeManager addHomeWithName:self.homeName.text completionHandler:^(HMHome *home, NSError *error)
    {
        
        if (!error) {
            NSLog(@"Created Home : %@",home.name);
            
        } else {
            
            NSLog(@"Error : %@",[error localizedDescription]);
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
